# Vim Masterclass

This project contains code from studying [Vim Masterclass](https://www.packtpub.com/application-development/vim-masterclass-video) course.

## 🏠 [Homepage](https://bitbucket.org/ahristov/vimmasterclass/src/master/)

## Author

👤 **Atanas Hristov**

## Notes

### Quick start

Modes:

- Normal mode
	- Anything you type in normal modeis a command.
- Insert mode
	- Activate from normal mode type "i"
	- Leave by pressing Escape
- Line mode
	- Activate from normal mode by typing ":"
	- Complete command to return to normal mode
	- Leave by pressing Escape

